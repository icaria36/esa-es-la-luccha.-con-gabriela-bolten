This song is DEMO quality. I cannot mix better than this. :) Here you have all the Bitwig project files.

https://gitlab.com/icaria36/esa-es-la-luccha.-con-gabriela-bolten/-/blob/master/exported/20210412/Esa%20es%20la%20luchha%20DEMO.mp3

This song uses samples extracted from 
https://gabrielaboltenrap.bandcamp.com/track/01-intro 
and sung by Gabriela Bolten. She generously gave me her permission to sample her track. ¡Muchas gracias, Gabriela! 


Compañeras\
Compañeras\
Compañeros\
Hoy estamos aquí por hacer una revolución

Queremos tierra, vivienda, trabajo, libertad\
Esa es la lucha

Compañeras\
Hoy luchamos\
Por la paz\
Por el amor\
Por la diversión\
Esa es la lucha

Compañero, compañera\
Alce su voz\
Alce su grito


ENGLISH TRANSLATION

Comrades\
Comrades\
Comrades\
Today we are here to make a revolution

We want land, housing, work, freedom\
This is our struggle

Comrades\
Today we fight\
For peace\
For love\
For fun\
This is our struggle

Comrade, comrade\
Rise your voice\
Rise your shout
